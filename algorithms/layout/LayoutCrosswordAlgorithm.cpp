#include "LayoutCrosswordAlgorithm.h"

/**
 * @return {bool} True if clients can build variant sized crossword
 */
bool LayoutCrosswordAlgorithm::supportsDynamicSizes() {
    return true;
}

/**
 * Build a crossword with the given size
 * 
 * @param {map<string, string>} wordsAndCluesMap Map from answers to their clues
 * @return {Crossword}
 */
Crossword* LayoutCrosswordAlgorithm::build(stringMap wordsAndCluesMap) {
    return build(wordsAndCluesMap, 15, 15);
}

/**
 * Build a crossword within the given dimensions with the given words and clues
 * 
 * @param {map<string, string>} wordsAndCluesMap Map from answers to their clues
 * @param {int} width
 * @param {int} height
 * @return {Crossword}
 */
Crossword* LayoutCrosswordAlgorithm::build(stringMap wordsAndCluesMap, int width, int height) {

    double thresh = 0.5;
    double density = 0;
    while (true) {
        Crossword* crossword = buildCrossword(wordsAndCluesMap, width, height);
        density = crossword->getDensity();
        if (density >= thresh) {// && crossword->hasEnoughIntersections()) {
            return crossword;
        }
        delete crossword;
    }

    //return NULL;
}

Crossword* LayoutCrosswordAlgorithm::buildCrossword(stringMap wordsAndCluesMap, int width, int height) {

    Crossword* crossword = new Crossword(width, height);

    std::vector<std::string> answers = Utilities::getKeysFromMap(wordsAndCluesMap);

    //Place first answer
    std::string firstAnswer = answers.back();
    answers.pop_back();
    CrosswordElement element = createElement(wordsAndCluesMap[firstAnswer], firstAnswer, 0, 0, ACROSS);
    crossword->placeElement(element);

    bool wordAdded = true;
    bool shouldIncludeEmptyAnswers = false;
    bool allowThreeLetterWords = false;

    while (wordAdded) {
        wordAdded = false;

        std::vector<std::string>::iterator it = answers.begin();
        while (it != answers.end()) {
            std::string currentAnswer = *it;
            std::string currentClue = wordsAndCluesMap[currentAnswer];

            if (currentClue.empty() && !shouldIncludeEmptyAnswers) {
                ++it;
                continue;
            }

            if (currentAnswer.size() == 3 && !allowThreeLetterWords) {
                ++it;
                continue;
            }

            bool wasAdded = tryToAddToCrossword(crossword, currentAnswer, currentClue);

            if (wasAdded) {
                wordAdded = true;
                it = answers.erase(it);
            } else {
                ++it;
            }
        }

        if (!wordAdded && !shouldIncludeEmptyAnswers) {
            //Pretend a word was added to continue the loop but flip the flag for including empty answers from now on
            shouldIncludeEmptyAnswers = true;
            wordAdded = true;
        }

        if (!wordAdded && shouldIncludeEmptyAnswers && !allowThreeLetterWords) {
            //If we can't find anything else then allow 3 letter filler words
            allowThreeLetterWords = true;
            wordAdded = true;
        }
    }
    crossword->finalize();
    return crossword;
}

/**
 * Attempt to add a clue-answer pair to the crossword
 * 
 * @param {Crossword} crossword Crossword to attempt to add a clue-answer pair to
 * @param {string} answer 
 * @param {string} clue
 * @return {bool} If the answer was successfully added to the crossword or not
 */
bool LayoutCrosswordAlgorithm::tryToAddToCrossword(
    Crossword* crossword, 
    std::string answer, 
    std::string clue) {

    //std::cout << "Trying to add " << answer << std::endl;
    std::vector<CrosswordElement> elementsInPuzzle = crossword->getElements();
    for(std::vector<CrosswordElement>::iterator it = elementsInPuzzle.begin(); it != elementsInPuzzle.end(); ++it) {
        if (addIfCommonLetter(crossword, *it, answer, clue)) {
            return true;
        }
    }
    return false;
}

bool LayoutCrosswordAlgorithm::addIfCommonLetter(
    Crossword* crossword, 
    CrosswordElement puzzleElement, 
    std::string answer, 
    std::string clue) {

    std::string puzzleString = puzzleElement.gridElement.word;
    //std::cout << "--- Inspecting ";
    //puzzleElement.print();
    int puzzleLetterIndex = 0;
    for (std::string::iterator puzzleElementIterator = puzzleString.begin(); puzzleElementIterator != puzzleString.end(); ++puzzleElementIterator) {
        //std::cout << "------ Looking at " << *puzzleElementIterator << std::endl;
        int pos = 0;
        std::size_t answerLetterIndex = 1;
        do {
            answerLetterIndex = answer.find_first_of(*puzzleElementIterator, pos);
            if (answerLetterIndex != std::string::npos &&
                addIfWordFits(crossword, puzzleElement, answer, clue, answerLetterIndex, puzzleLetterIndex)) {
                return true;
            }
            pos = answerLetterIndex + 1;
        } while (answerLetterIndex != std::string::npos);
        puzzleLetterIndex++;
    }
    return false;
}

bool LayoutCrosswordAlgorithm::addIfWordFits(
    Crossword* crossword, 
    CrosswordElement puzzleElement, 
    std::string answer, 
    std::string clue, 
    size_t answerLetterIndex, 
    size_t puzzleLetterIndex) {

    //Found letter from puzzle in answer
    Direction dir = puzzleElement.gridElement.direction;
    CrosswordElement possibleElement;
    if (dir == ACROSS) {
        possibleElement = createElement(clue, answer, puzzleElement.gridElement.row - answerLetterIndex, puzzleElement.gridElement.column + puzzleLetterIndex, DOWN);
    } else {
        possibleElement = createElement(clue, answer, puzzleElement.gridElement.row + puzzleLetterIndex, puzzleElement.gridElement.column - answerLetterIndex, ACROSS);
    }

    //std::cout << "------ Trying to add ";
    //possibleElement.print();

    if(crossword->placeElement(possibleElement)) {
        //std::cout << "+++SUCCESS+++" << std::endl;
        return true;
    }
    
    return false;
}

CrosswordElement LayoutCrosswordAlgorithm::createElement(
    std::string clue, 
    std::string answer, 
    int row, 
    int col, 
    Direction direction) {

    GridElement gridElement(answer, row, col, direction);
    CrosswordElement crosswordElement(-1, clue, gridElement, 0);
    return crosswordElement;
}