#ifndef LAYOUTCROSSWORDALGORITHM
#define LAYOUTCROSSWORDALGORITHM

#include <map>
#include <vector>
#include <string>

#include "Crossword.h"
#include "CrosswordBuilderAlgorithm.h"
#include "CrosswordElement.h"
#include "Common.h"
#include "Utilities.h"

class LayoutCrosswordAlgorithm : public CrosswordBuilderAlgorithm {
private:
    Crossword* buildCrossword(stringMap wordsAndCluesMap, int width, int height);
    bool tryToAddToCrossword(Crossword* crossword, std::string answer, std::string clue);
    CrosswordElement createElement(std::string clue, std::string answer, int row, int col, Direction direction);
    bool addIfCommonLetter(Crossword* crossword, CrosswordElement puzzleElement, std::string answer, std::string clue);
    bool addIfWordFits(Crossword* crossword, CrosswordElement puzzleElement, std::string answer, std::string clue, size_t answerLetterIndex, size_t puzzleLetterIndex);

public:
    Crossword* build(stringMap wordsAndCluesMap);
    Crossword* build(stringMap wordsAndCluesMap, int width, int height);
    bool supportsDynamicSizes();
};

#endif
