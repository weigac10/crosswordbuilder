#include "TemplateCrosswordAlgorithm.h"

Crossword* TemplateCrosswordAlgorithm::build(stringMap wordsAndCluesMap) {
    Crossword* crosswordTemplate = CrosswordTemplates::templates.front(); //TODO: grab a random template
    std::map<int, std::vector<std::string>> lengthMap = Utilities::getLengthToWordsMap(wordsAndCluesMap);

    return this->buildForTemplate(wordsAndCluesMap, lengthMap, crosswordTemplate);
}

Crossword* TemplateCrosswordAlgorithm::buildForTemplate(
    stringMap wordsAndCluesMap, 
    std::map<int, std::vector<std::string>> lengthMap,
    Crossword* crosswordTemplate) {

    Crossword* crossword = new Crossword(15, 15);
    std::vector<CrosswordElement> elements = crosswordTemplate->getElements();

    int wordsFilled = 0;
    for (std::vector<CrosswordElement>::iterator it = elements.begin(); it != elements.end(); ++it) {
        int lengthNeeded = it->gridElement.word.size();
        std::vector<std::string> elementsOfLength = lengthMap[lengthNeeded];

        bool foundWordForSpot = false;
        std::random_shuffle(elementsOfLength.begin(), elementsOfLength.end());
        for (std::vector<std::string>::iterator i = elementsOfLength.begin(); i != elementsOfLength.end(); ++i) {
            CrosswordElement possibleElement = *it;
            possibleElement.clue = wordsAndCluesMap[*i];
            possibleElement.gridElement.word = *i;

            if(crossword->placeElement(possibleElement)) {
                foundWordForSpot = true;
                break;
            }
        }

        if (!foundWordForSpot) {
            //Prompt user to fill it in?
        } else {
            wordsFilled++;
        }
    }

    crossword->finalize();
    return crossword;
    
}