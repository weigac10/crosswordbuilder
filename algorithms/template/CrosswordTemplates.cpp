#include "CrosswordTemplates.h"

std::vector<Crossword*> CrosswordTemplates::buildTemplates() {    
    std::vector<bool2d> temps {
        {
            {1,1,1,1,1,1,1,0,1,1,1,1,1,1,1},
            {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
            {1,1,1,1,1,0,1,1,1,1,1,1,1,1,1},
            {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
            {0,1,1,1,1,1,1,1,1,0,1,1,1,1,1},
            {1,0,1,0,1,0,1,0,1,0,0,0,1,0,0},
            {1,1,1,1,1,1,1,1,0,1,1,1,1,1,1},
            {1,0,0,0,1,0,1,0,1,0,1,0,0,0,1},
            {1,1,1,1,1,1,0,1,1,1,1,1,1,1,1},
            {0,0,1,0,0,0,1,0,1,0,1,0,1,0,1},
            {1,1,1,1,1,0,1,1,1,1,1,1,1,1,0},
            {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
            {1,1,1,1,1,1,1,1,1,0,1,1,1,1,1},
            {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
            {1,1,1,1,1,1,1,0,1,1,1,1,1,1,1}
        }
    };

    std::vector<Crossword*> crosswords;
    for (std::vector<bool2d>::iterator it = temps.begin(); it != temps.end(); ++it) {
        Crossword* crossword = CrosswordTemplates::generateEmptyCrosswordFromTemplate(*it);
        crosswords.push_back(crossword);
    }
    return crosswords;
}

Crossword* CrosswordTemplates::generateEmptyCrosswordFromTemplate(bool2d crosswordTemplate) {
    Crossword* crossword = new Crossword(15, 15);

    CrosswordElement curAcross;
    curAcross.gridElement.direction = ACROSS;
    CrosswordElement curDown;
    curDown.gridElement.direction = DOWN;

    for (int index1=0; index1<15; index1++) {
        CrosswordTemplates::addAndResetElement(crossword, curAcross);
        CrosswordTemplates::addAndResetElement(crossword, curDown);
        for (int index2=0; index2<15; index2++) {
            bool isWhiteSquareAcross = crosswordTemplate[index1][index2];
            if (!isWhiteSquareAcross) {
                CrosswordTemplates::addAndResetElement(crossword, curAcross);
            } else {
                CrosswordTemplates::appendToElement(curAcross, index1, index2);
            }

            bool isWhiteSquareDown = crosswordTemplate[index2][index1];
            if (!isWhiteSquareDown) {
                CrosswordTemplates::addAndResetElement(crossword, curDown);
            } else {
                CrosswordTemplates::appendToElement(curDown, index2, index1);
            }
        }
    }

    CrosswordTemplates::addAndResetElement(crossword, curAcross);
    CrosswordTemplates::addAndResetElement(crossword, curDown);
    
    return crossword;
}

void CrosswordTemplates::addAndResetElement(Crossword* crossword, CrosswordElement& elem) {
    if (elem.gridElement.word.size() > 1) {
        crossword->placeElement(elem);
    }
    CrosswordElement newElement;
    newElement.gridElement.direction = elem.gridElement.direction;
    elem = newElement;
}

void CrosswordTemplates::appendToElement(CrosswordElement& elem, int row, int col) {
    if (elem.gridElement.word.size() == 0) {
        elem.gridElement.row = row;
        elem.gridElement.column = col;
    }
    elem.gridElement.word += DEFAULT;
}

std::vector<Crossword*> CrosswordTemplates::templates = CrosswordTemplates::buildTemplates();