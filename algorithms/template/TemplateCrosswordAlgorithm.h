#ifndef TEMPLATECROSSWORDALGORITHM
#define TEMPLATECROSSWORDALGORITHM

#include <map>
#include <vector>
#include <string>
#include <random>

#include "Crossword.h"
#include "CrosswordTemplates.h"
#include "CrosswordBuilderAlgorithm.h"
#include "CrosswordElement.h"
#include "Common.h"

class TemplateCrosswordAlgorithm : public CrosswordBuilderAlgorithm {
private:
    Crossword* buildForTemplate(stringMap wordsAndCluesMap, std::map<int, std::vector<std::string>> lengthMap, Crossword* crosswordTemplate);

public:
    Crossword* build(stringMap wordsAndCluesMap);
};

#endif
