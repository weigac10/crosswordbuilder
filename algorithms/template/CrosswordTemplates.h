#ifndef CROSSWORDTEMPLATES
#define CROSSWORDTEMPLATES

#include <vector>

#include "Crossword.h"
#include "Common.h"
#include "CrosswordElement.h"

typedef std::vector<std::vector<bool>> bool2d;

class CrosswordTemplates {
public:
    static std::vector<Crossword*> templates;
private:
    static const char DEFAULT = ' ';

    static std::vector<Crossword*> buildTemplates();
    static Crossword* generateEmptyCrosswordFromTemplate(bool2d crosswordTemplate);
    static void addAndResetElement(Crossword* crossword, CrosswordElement& elem);
    static void appendToElement(CrosswordElement& elem, int row, int col);
};

#endif