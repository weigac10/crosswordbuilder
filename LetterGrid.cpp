#include "LetterGrid.h"

/**
 * Create a letter grid of size w by h
 * 
 * @param {int} w Width
 * @param {int} h Height
 */
LetterGrid::LetterGrid(int w, int h) {
    this->width = w;
    this->height = h;

    this->grid = new char*[this->height];
    for (int i = 0; i < this->height; i++) {
        this->grid[i] = new char[this->width];
        for (int j = 0; j < this->width; j++) {
            this->grid[i][j] = DEFAULT;
        }
    }
}

LetterGrid& LetterGrid::operator=(const LetterGrid& other) {
    cleanup();
    this->width = other.width;
    this->height = other.height;

    this->grid = new char*[this->height];
    for (int i = 0; i < this->height; i++) {
        this->grid[i] = new char[this->width];
        for (int j = 0; j < this->width; j++) {
            this->grid[i][j] = other.grid[i][j];
        }
    }
    return *this;
}

LetterGrid::~LetterGrid() {
    cleanup();
}

void LetterGrid::cleanup() {
    for (int i = 0; i < this->height; i++) {
        delete[] grid[i];
    }
    delete[] grid;
}

/**
 * Get the density of the letter grid (number of filled squares divided by total squares)
 * 
 * @return {double} Density
 */
double LetterGrid::getDensity() {
    double numSpaces = this->width * this->height;
    int numFilled = 0;
    for (int r=0; r<this->height; r++) {
        for (int c=0; c<this->width; c++) {
            if(this->grid[r][c] != DEFAULT) {
                numFilled++;
            }
        }
    }
    return numFilled / numSpaces;
}

/**
 * @return {char**}
 */
char** LetterGrid::getGrid() {
    return this->grid;
}

/**
 * Place a GridElement in the letter grid
 * If the element does not fit in the grid, false will be returned and the grid will not be modified
 * 
 * @param {GridElement}
 * @return {bool} Whether the placement was successful or not
 */
bool LetterGrid::placeElement(GridElement element) {
    if (isValidElement(element)) {
        if (element.direction == ACROSS) {
            for (int i=0; i<element.word.size(); i++) {
                this->grid[element.row][element.column + i] = element.word[i];
            }
        } else if (element.direction == DOWN) {
            for (int i=0; i<element.word.size(); i++) {
                this->grid[element.row + i][element.column] = element.word[i];
            }
        }
        return true;
    }
    return false;
}

/**
 * Returns true if the element fits in the letter grid
 * Does not take into account if the element overwrites other elements
 * 
 * @param {GridElement}
 * @return {bool} True if the element fits in the grid
 */
bool LetterGrid::isValidElement(GridElement element) {
    bool isValidStartingRow = element.row >= 0 && element.row < this->height;
    bool isValidStartingColumn = element.column >= 0 && element.column < this->width;

    bool wordFitsInBounds;
    if (element.direction == ACROSS) {
        wordFitsInBounds = element.column + element.word.size() - 1 < this->width;
    } else if (element.direction == DOWN) {
        wordFitsInBounds = element.row + element.word.size() - 1 < this->height;
    } else {
        wordFitsInBounds = false; //No direction
    }

    return isValidStartingRow && isValidStartingColumn && wordFitsInBounds;
}

/**
 * Returns true if the given element appropriately overlaps the current elements in the grid
 * (i.e. all letters match up)
 * 
 * @param {GridElement}
 * @return {bool} True if letters overlap properly
 */
bool LetterGrid::doLettersOverlapProperly(GridElement element) {
    if (!isValidElement(element)) {
        return false;
    }

    bool isValid = true;
    if (element.direction == ACROSS) {
        for (int i=0; i<element.word.size(); i++) {
            char gridChar = this->grid[element.row][element.column + i];
            bool isSameLetter = (gridChar == element.word[i]);
            isValid = isValid && (isSameLetter || gridChar == DEFAULT);
        }
    } else if (element.direction == DOWN) {
        for (int i=0; i<element.word.size(); i++) {
            char gridChar = this->grid[element.row + i][element.column];
            bool isSameLetter = (gridChar == element.word[i]);
            isValid = isValid && (isSameLetter || gridChar == DEFAULT);
        }
    }
    return isValid;
}

/**
 * Print the letter grid to stdout
 */
void LetterGrid::draw() const {
    for (int r=0; r<this->height; r++) {
        for (int c=0; c<this->width; c++) {
            std::cout << this->grid[r][c] << " ";
        }
        std::cout << std::endl;
    }
}