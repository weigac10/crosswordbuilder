#include "CrosswordBuilder.h"

//I had a lot more planned for this class...

CrosswordBuilder::CrosswordBuilder(CrosswordBuilderAlgorithm* alg) {
    this->builderAlgorithm = alg;
}

CrosswordBuilder::~CrosswordBuilder() {
    this->builderAlgorithm = NULL;
}

Crossword* CrosswordBuilder::build(stringMap wordsAndCluesMap) {
    return this->builderAlgorithm->build(wordsAndCluesMap);
}

Crossword* CrosswordBuilder::build(stringMap wordsAndCluesMap, int width, int height) {
    if (this->builderAlgorithm->supportsDynamicSizes()) {
        return this->builderAlgorithm->build(wordsAndCluesMap, width, height);
    } else {
        //Ignoring width and height passed in
        return this->build(wordsAndCluesMap);
    }
}