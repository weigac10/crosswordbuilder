#include "ValidPositionTemplate.h"

ValidPositionTemplate::ValidPositionTemplate() {
    buildTemplates();
}

ValidPositionTemplate::~ValidPositionTemplate() {

}

void ValidPositionTemplate::buildTemplates() {
    //Templates specifying where words/letters are allowed
    //E.g. acrossFirst -
    // For a given word (that is located in the middle row below), 
    //  where can another word start around it
    //
    //  - D D D D D D A
    //  - D D D D D D -
    //  D - - - - - - B
    //
    // We duplicate the middle column of the template across the length of the word
    // D means a word may begin there if it is DOWN
    // A means a word may begin there if it is ACROSS
    // B means a word my beging there if it is either ACROSS or DOWN (both)

    int acrossFirst[3][3] = {
        {NONE, DOWN, ACROSS},
        {NONE, DOWN, NONE},
        {DOWN, NONE, ACROSS | DOWN}
    };

    int acrossMiddle[3][3] = {
        {NONE, DOWN, NONE},
        {NONE, DOWN, NONE},
        {NONE, DOWN, NONE}
    };

    int acrossLast[3][3] = {
        {ACROSS|DOWN, NONE, DOWN},
        {NONE, DOWN, NONE},
        {ACROSS, DOWN, NONE}
    };

    int downFirst[3][3] = {
        {NONE, NONE, ACROSS},
        {ACROSS, ACROSS, NONE},
        {DOWN, NONE, ACROSS | DOWN}
    };

    int downMiddle[3][3] = {
        {NONE, NONE, NONE},
        {ACROSS, ACROSS, ACROSS},
        {NONE, NONE, NONE}
    };

    int downLast[3][3] = {
        {ACROSS|DOWN, NONE, DOWN},
        {NONE, ACROSS, ACROSS},
        {ACROSS, NONE, NONE}
    };

    templates[std::make_pair(ACROSS, FIRST)] = new int*[3];
    templates[std::make_pair(ACROSS, MIDDLE)] = new int*[3];
    templates[std::make_pair(ACROSS, LAST)] = new int*[3];
    templates[std::make_pair(DOWN, FIRST)] = new int*[3];
    templates[std::make_pair(DOWN, MIDDLE)] = new int*[3];
    templates[std::make_pair(DOWN, LAST)] = new int*[3];

    for (int i=0; i<3; i++) {
        templates[std::make_pair(ACROSS, FIRST)][i] = new int[3];
        templates[std::make_pair(ACROSS, MIDDLE)][i] = new int[3];
        templates[std::make_pair(ACROSS, LAST)][i] = new int[3];
        templates[std::make_pair(DOWN, FIRST)][i] = new int[3];
        templates[std::make_pair(DOWN, MIDDLE)][i] = new int[3];
        templates[std::make_pair(DOWN, LAST)][i] = new int[3];

        for (int j=0; j<3; j++) {
            templates[std::make_pair(ACROSS, FIRST)][i][j] = acrossFirst[i][j];
            templates[std::make_pair(ACROSS, MIDDLE)][i][j] = acrossMiddle[i][j];
            templates[std::make_pair(ACROSS, LAST)][i][j] = acrossLast[i][j];
            templates[std::make_pair(DOWN, FIRST)][i][j] = downFirst[i][j];
            templates[std::make_pair(DOWN, MIDDLE)][i][j] = downMiddle[i][j];
            templates[std::make_pair(DOWN, LAST)][i][j] = downLast[i][j];        
        }
    }

}

int** ValidPositionTemplate::get(Direction dir, LetterPosition pos, int length) {
    int** temp = templates[std::make_pair(dir, pos)];
    switch (dir) {
        case ACROSS:
            return buildAcrossTemplate(temp, length);
        break;
        case DOWN:
            return buildDownTemplate(temp, length);
        break;
        default:
            return NULL;
    }
}

int** ValidPositionTemplate::buildAcrossTemplate(int** temp, int length) {
    int** directionsTemplate = new int*[3];
    for (int i=0; i<3; i++) {
        directionsTemplate[i] = new int[length+2];
    }
    directionsTemplate[0][0] = temp[0][0];
    directionsTemplate[1][0] = temp[1][0];
    directionsTemplate[2][0] = temp[2][0];
    directionsTemplate[0][length+1] = temp[0][2];
    directionsTemplate[1][length+1] = temp[1][2];
    directionsTemplate[2][length+1] = temp[2][2];
    for (int i=1; i<length+1; i++) {
        directionsTemplate[0][i] = temp[0][1];
        directionsTemplate[1][i] = temp[1][1];
        directionsTemplate[2][i] = temp[2][1];
    }

    return directionsTemplate;
}

int** ValidPositionTemplate::buildDownTemplate(int** temp, int length) {
    int** directionsTemplate = new int*[length+2];
    for (int i=0; i<length+2; i++) { 
        directionsTemplate[i] = new int[3];
    }
    directionsTemplate[0][0] = temp[0][0];
    directionsTemplate[0][1] = temp[0][1];
    directionsTemplate[0][2] = temp[0][2];
    directionsTemplate[length+1][0] = temp[2][0];
    directionsTemplate[length+1][1] = temp[2][1];
    directionsTemplate[length+1][2] = temp[2][2];
    for (int i=1; i<length+1; i++) {
        directionsTemplate[i][0] = temp[1][0];
        directionsTemplate[i][1] = temp[1][1];
        directionsTemplate[i][2] = temp[1][2];
    }


    return directionsTemplate;
}