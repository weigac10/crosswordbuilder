#ifndef CROSSWORD
#define CROSSWORD

#include <vector>
#include <iostream>

#include "CrosswordElement.h"
#include "LetterGrid.h"
#include "validPositionTracker.h"

class Crossword : public LetterGrid {
private:
    std::vector<CrosswordElement> elements;
    ValidPositionTracker* validPositionTracker;

public:
    Crossword(int w, int h);
    Crossword& operator=(const Crossword& other);
    ~Crossword();
    void cleanup();
    bool placeElement(CrosswordElement element);
    bool isValidElement(CrosswordElement element);
    std::vector<CrosswordElement> getElements() const;
    void drawValidDirections() const;
    void draw() const;
    bool hasEnoughIntersections();
    void calculateIntersections();
    void finalize();
    void printAsJSON() const;
};

#endif