#ifndef CROSSWORDELEMENT
#define CROSSWORDELEMENT

#include "Common.h"

#include <string>

class CrosswordElement {
public:
    int number;
    int intersections;

    std::string clue;

    GridElement gridElement;

    CrosswordElement() {
        number = -1;
        intersections = 0;
        clue = "";
        GridElement gE("", 0, 0, NONE);
        gridElement = gE;
    }

    CrosswordElement(int n, std::string c, GridElement gE, int i) : number(n), intersections(i), clue(c), gridElement(gE) {
    }

    bool hasEnoughIntersections() {
        //If the length of the word is at least the number of intersections squared
        return this->gridElement.word.size() >= this->intersections * this->intersections;
    }

    void print() const {
        std::string dirString;
        if (gridElement.direction == ACROSS) {
            dirString = "A";
        } else if (gridElement.direction == DOWN) {
            dirString = "D";
        } else {
            dirString = "?";
        }
        std::cout << number << dirString << ": " << gridElement.word << " (" << gridElement.row << ", " << gridElement.column << ")" << std::endl;
    }

    void printAsJSON() const {
        std::cout << "{";
        std::cout << "\"number\":" << number << ",";
        std::cout << "\"direction\":\"" << (gridElement.direction == DOWN? "down" : "across") << "\",";
        std::cout << "\"row\":" << gridElement.row << ",";
        std::cout << "\"column\":" << gridElement.column << ",";
        std::cout << "\"clue\":\"" << clue << "\",";
        std::cout << "\"answer\":\"" << gridElement.word << "\"";
        std::cout << "}";
    }

    bool operator< (const CrosswordElement& other) const {
        if (gridElement.direction == ACROSS && other.gridElement.direction == DOWN) {
            return true;
        } else if (gridElement.direction == DOWN && other.gridElement.direction == ACROSS) {
            return false;
        } else {
            return (number < other.number);
        }
    }
};

#endif