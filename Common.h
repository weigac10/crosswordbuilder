#ifndef COMMON
#define COMMON

#include <string>
#include <iostream>

enum Direction {
    NONE = 0,
    ACROSS = 1<<0,
    DOWN = 1<<1
};

enum LetterPosition {
    FIRST = 0,
    MIDDLE = 1,
    LAST = 2
};

typedef struct GridElement {
    std::string word;
    int row;
    int column;
    Direction direction;

    GridElement () {
        this->word = "";
        this->row = -1;
        this->column = -1;
        this->direction = NONE;
    }

    GridElement (std::string w, int r, int c, Direction d) {
        this->word = w;
        this->row = r;
        this->column = c;
        this->direction = d;
    }

    GridElement (GridElement const &elem) {
        this->word = elem.word;
        this->row = elem.row;
        this->column = elem.column;
        this->direction = elem.direction;
    }

} GridElement;

#endif