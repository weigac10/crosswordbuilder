#ifndef VALIDPOSITIONTRACKER
#define VALIDPOSITIONTRACKER
#include <list>
#include <iostream>

#include "Common.h"
#include "ValidPositionTemplate.h"

class ValidPositionTracker {
private:
    int width;
    int height;

    ValidPositionTemplate validPositionTemplate;
    int** validStartDirections;
    int** validMiddleDirections;
    int** validEndDirections;

    int** initializeTemplate(int w, int h);
    int** initializeTemplate(int w, int h, int** toCopy);

    void markInvalidDirectionsAcross(GridElement element);
    void markInvalidDirectionsDown(GridElement element);

    bool isValidDirectionIndex(int row, int column);

    GridElement getNextElement(GridElement element, int index);

public:
    ValidPositionTracker(int w, int h);
    ValidPositionTracker(const ValidPositionTracker& other);
    ~ValidPositionTracker();

    bool isValidElement(GridElement element);
    void markInvalidDirections(GridElement element);
    void draw();
};

#endif