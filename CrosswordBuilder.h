#ifndef CROSSWORDBUILDER
#define CROSSWORDBUILDER

#include <map>
#include <string>
#include <vector>

#include "Crossword.h"
#include "CrosswordBuilderAlgorithm.h"
#include "Utilities.h"

class CrosswordBuilder {
private:
    CrosswordBuilderAlgorithm* builderAlgorithm;

public:
    CrosswordBuilder(CrosswordBuilderAlgorithm* alg);
    ~CrosswordBuilder();
    Crossword* build(stringMap wordsAndCluesMap);
    Crossword* build(stringMap wordsAndCluesMap, int width, int height);
};

#endif
