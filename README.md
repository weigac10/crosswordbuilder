# Crossword Builder
## Created by Christian Weigandt

This code will create a crossword similar to the following:

[Generated crossword](images/crossword.png)

The algorithm is a bit brute force at the moment but will eventually create a crossword with over 50% density within a 15x15 grid.

In order to build:

1. Create directory 'Outputs'
2. make
3. make run
4. Later updates will include JS for displaying json.txt output file
