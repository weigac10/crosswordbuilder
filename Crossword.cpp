#include "Crossword.h"

/**
 * Create a Crossword with the given dimensions
 *
 * @param {int} w Width
 * @param {int} h Height
 */
Crossword::Crossword(int w, int h) : LetterGrid(w, h) {
    this->validPositionTracker = new ValidPositionTracker(w, h);
}

Crossword& Crossword::operator=(const Crossword& other) {
    cleanup();

    this->validPositionTracker = new ValidPositionTracker(*(other.validPositionTracker));

    for (std::vector<CrosswordElement>::iterator it = other.getElements().begin(); it != other.getElements().end(); ++it) {
        elements.push_back(*it);
    }
    return *this;
}

Crossword::~Crossword() {
    cleanup();
}

void Crossword::cleanup() {
    //HACK
    if (validPositionTracker != NULL) {
        delete validPositionTracker;
        validPositionTracker = NULL;
    }
    this->elements.clear(); 
}

/**
 * Place an element into the crossword
 * Will first check if the element is valid, meaning that it does not break any rules of a crossword entry
 * 
 * @param {CrosswordElement}
 * @return {bool} True if the element was successfully added to the crossword
 */
bool Crossword::placeElement(CrosswordElement element) {
    if (!isValidElement(element)) {
        return false;
    }
    LetterGrid::placeElement(element.gridElement);
    this->elements.push_back(element);
    validPositionTracker->markInvalidDirections(element.gridElement);
    return true;
}

/**
 * Get the elements in the crossword
 * 
 * @return {std::vector<CrosswordElement>}
 */
std::vector<CrosswordElement> Crossword::getElements() const {
    return this->elements;
}

/**
 * Returns true if the element fits in the crossword bounds and does not border any existing elements
 * 
 * @param {CrosswordElement}
 * @return {bool} True if the element is valid in the crossword
 */
bool Crossword::isValidElement(CrosswordElement element) {
    return LetterGrid::isValidElement(element.gridElement) && 
        validPositionTracker->isValidElement(element.gridElement) &&
        doLettersOverlapProperly(element.gridElement);
}

/**
 * Print the valid direction matrices to stdout
 */
void Crossword::drawValidDirections() const {
    this->validPositionTracker->draw();
}

/**
 * Once all elements have been added to the crossword, finalize should be called
 * Assigns numbers to all elements in the crossword
 */
void Crossword::finalize() {
    //Super lazy way of doing this
    int number = 1;
    for (int r=0; r<height; r++) {
        for (int c=0; c<width; c++) {
            bool found = false;
            for (std::vector<CrosswordElement>::iterator it = elements.begin(); it != elements.end(); ++it) {
                CrosswordElement elem = *it;
                if (elem.gridElement.row == r && elem.gridElement.column == c) {
                    (*it).number = number;
                    found = true;
                }
            }
            if (found) {
                number++;
            }
        }
    }
    this->calculateIntersections();
    std::sort(elements.begin(), elements.end());
}

void Crossword::calculateIntersections() {

    //Build matrix of 0's, we will fill it with number of words at each location (0, 1, or 2)
    int** letterCount = new int*[this->height];
    for (int i = 0; i < this->height; i++) {
        letterCount[i] = new int[this->width];
        for (int j = 0; j < this->width; j++) {
            letterCount[i][j] = 0;
        }
    }

    //Loop over each element and increment each location it passes through
    for (std::vector<CrosswordElement>::const_iterator it = elements.begin(); it != elements.end(); ++it) {
        GridElement element = it->gridElement;
        if (element.direction == ACROSS) {
            for (int i=0; i<element.word.size(); i++) {
                letterCount[element.row][element.column + i]++;
            }
        } else if (element.direction == DOWN) {
            for (int i=0; i<element.word.size(); i++) {
                letterCount[element.row + i][element.column]++;
            }
        }
    }

    //Loop over each element and see how many 2's exist for their location
    for (std::vector<CrosswordElement>::iterator it = elements.begin(); it != elements.end(); ++it) {
        GridElement element = it->gridElement;
        if (element.direction == ACROSS) {
            for (int i=0; i<element.word.size(); i++) {
                if (letterCount[element.row][element.column + i] == 2) {
                    (*it).intersections++;
                }
            }
        } else if (element.direction == DOWN) {
            for (int i=0; i<element.word.size(); i++) {
                if (letterCount[element.row + i][element.column] == 2) {
                    (*it).intersections++;
                }
            }
        }
    }

    for (int i = 0; i < this->height; i++) {
        delete[] letterCount[i];
    }
    delete[] letterCount;
}

bool Crossword::hasEnoughIntersections() {
    for (std::vector<CrosswordElement>::const_iterator it = elements.begin(); it != elements.end(); ++it) {
        CrosswordElement element = *it;
        if (!element.hasEnoughIntersections()) {
            return false;
        }
    }
    return true;
}

/**
 * Print the crossword to stdout
 */
void Crossword::draw() const {
    LetterGrid::draw();
    std::cout << std::endl;
    for (std::vector<CrosswordElement>::const_iterator it = elements.begin(); it != elements.end(); ++it) {
        (*it).print();
    }
}

/**
 * Print the crossword as JSON
 */
void Crossword::printAsJSON() const {
    std::cout << "{" << std::endl;
    std::cout << "\"width\":" << width << "," << std::endl;
    std::cout << "\"height\":" << height << "," << std::endl;
    std::cout << "\"puzzle\":" << "[" << std::endl;
    for (int r=0; r<height; r++) {
        std::cout << "[";
        for (int c=0; c<width; c++) {
            std::cout << "\"" << grid[r][c] << "\"";
            if (c < width - 1) {
                std::cout << ",";
            }
        }
        std::cout << "]";
        if (r < height - 1) {
            std::cout << ",";
        }
        std::cout << std::endl;
    }
    std::cout << "]";
    std::cout << "," << std::endl;

    std::cout << "\"elements\":" << "[" << std::endl;
    for (std::vector<CrosswordElement>::const_iterator it = elements.begin(); it != elements.end(); ++it) {
        (*it).printAsJSON();
        if (it != elements.end() - 1) {
            std::cout << "," << std::endl;
        }
    }
    std::cout << "]" << std::endl;
    std::cout << "}" << std::endl;
}