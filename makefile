CC=g++
CFLAGS=-c -Wall -std=c++11 $(addprefix -I, $(INCLUDES))
DFLAGS=-DDEBUG -g
MKDIR=mkdir -p
INCLUDES=. algorithms/template algorithms/layout
WORDSFILE=finalWords
JSONFILE=json.js
SRC=main.cpp Crossword.cpp CrosswordBuilder.cpp LetterGrid.cpp ValidPositionTemplate.cpp ValidPositionTracker.cpp
LAYOUT_SRC=algorithms/layout/LayoutCrosswordAlgorithm.cpp
TEMPLATE_SRC=algorithms/template/TemplateCrosswordAlgorithm.cpp algorithms/template/CrosswordTemplates.cpp
OUTDIR=Outputs/
OUTFILE=$(OUTDIR)main
OBJ=$(SRC:.cpp=.o)

all:	CFLAGS+=$(DFLAGS)
all:	layout

main: $(OBJ)
	$(CC) $(addprefix $(OUTDIR), $(OBJ)) -o $(OUTFILE)

%.o: %.cpp
	$(CC) $(CFLAGS) $< -o $(OUTDIR)$@

run:
	./$(OUTFILE) < $(WORDSFILE) > $(JSONFILE)

runStdout:
	./$(OUTFILE) < $(WORDSFILE)

runDebug:
	lldb $(OUTFILE)

debug: CFLAGS+=$(DFLAGS)
debug: main

layout:	makeOutputDirectories
layout:	CFLAGS+=-DLAYOUT_ALGORITHM
layout: SRC+=$(LAYOUT_SRC)
layout: $(LAYOUT_SRC:.cpp=.o)
layout:	main

template:	makeOutputDirectories
template:	CFLAGS+=-DTEMPLATE_ALGORITHM
template:	SRC+=$(TEMPLATE_SRC) 
template:	$(TEMPLATE_SRC:.cpp=.o)
template:	main

makeOutputDirectories:
	$(MKDIR) $(OUTDIR)algorithms/template
	$(MKDIR) $(OUTDIR)algorithms/layout

clean:
	-/bin/rm -rf $(OUTDIR)*
