#ifndef VALIDPOSITIONTEMPLATE
#define VALIDPOSITIONTEMPLATE

#include <map>
#include <utility>

#include "Common.h"

class ValidPositionTemplate { 
private:
    std::map<std::pair<Direction, LetterPosition>, int**> templates;
    void buildTemplates();
    int** buildAcrossTemplate(int** temp, int length);
    int** buildDownTemplate(int** temp, int length);

public:
    ValidPositionTemplate();
    ~ValidPositionTemplate();
    int** get(Direction dir, LetterPosition pos, int length);
};

#endif