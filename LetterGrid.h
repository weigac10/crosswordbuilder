#ifndef LETTERGRID
#define LETTERGRID

#include "Common.h"
#include <iostream>

class LetterGrid {
protected:
    bool isValidElement(GridElement element);
    int width;
    int height;
    char** grid;

    static const char DEFAULT = ' ';

public:
    LetterGrid(int w, int h);
    LetterGrid& operator=(const LetterGrid& other);
    ~LetterGrid();
    void cleanup();
    char** getGrid();
    bool placeElement(GridElement element);
    bool doLettersOverlapProperly(GridElement element);
    void draw() const;
    double getDensity();
};

#endif


