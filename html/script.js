function createBoard(info) {
  var board = document.getElementById("crossword");
  for (var i=0; i<info.height; i++) {
    var row = createRow(i, info);
    board.appendChild(row);
  }
}

function createRow(rowNumber, info) {
  var row = document.createElement("div");
  row.classList = "row";
  for (var i=0; i<info.width; i++) {
    var block = createBlock(rowNumber, i, info);
    row.appendChild(block);
  }
  return row;
}

function createBlock(row, column, info) {
  var block = document.createElement("div");
  block.classList = "block";
  
  var letter = info.puzzle[row][column];
    var letterSpan = document.createElement("div");
    letterSpan.classList = "letter";
    letterSpan.innerHTML = letter;
    block.appendChild(letterSpan);
  if (letter === " ") {
     block.classList += " no-letter";
  }
  
  var numberDiv = getNumberDiv(row, column, info);
  if (numberDiv) {
    block.appendChild(numberDiv);
  }
  
  return block;
}

function getNumberDiv(row, column, info) {
  var numberObj;
  info.elements.forEach(function (element) {
    if (element.row === row && element.column === column) {
      numberObj = element;
    }
  });
  
  var div;
  if (numberObj) {
    div = document.createElement("div");
    div.classList = "number";
    div.innerHTML = numberObj.number;
  }
  return div;
}

function writeClues(puzzle) {
  var acrossCluesDiv = document.getElementById("acrossClues");
  var downCluesDiv = document.getElementById("downClues");
  
  puzzle.elements.forEach(function (element) {
    var clueDiv = document.createElement("div");
    clueDiv.classList = "clue";

    var parts = element.answer.split(' ');
    var lenString = '';
    parts.forEach(function (part, i) {
      if (i !== 0) {
        lenString += ", ";
      }
      lenString += part.length;

    })
    
    var clue = "";
    clue += element.number;
    clue += element.direction==="across"?"A":"D";
    clue += ": ";
    clue += element.clue;
    clue += " (" + lenString + ")";
    clueDiv.innerHTML = clue;
    
    if (element.hasOwnProperty("done")  && !element.done) {
      clueDiv.style.color='red';
    }
    
    if (element.direction === "across") {
      acrossCluesDiv.appendChild(clueDiv);
    } else {
      downCluesDiv.appendChild(clueDiv);
    }
  });
}