#ifndef UTILITIES
#define UTILITIES

#include <algorithm>
#include <random>

#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#include <chrono>       // std::chrono::system_clock

#include <vector>
#include <map>
#include <string>

typedef std::map<std::string, std::string> stringMap;

class Utilities {
public:
    static std::vector<std::string> getKeysFromMap(stringMap map) {
        unsigned randSeed = std::chrono::system_clock::now().time_since_epoch().count();

        std::vector<std::string> v;
        for(std::map<std::string,std::string>::iterator it = map.begin(); it != map.end(); ++it) {
            v.push_back(it->first);
        }
        std::shuffle(v.begin(), v.end(), std::default_random_engine(randSeed));
        return v;
    }

    static std::map<int, std::vector<std::string>> getLengthToWordsMap(stringMap map) {
        std::map<int, std::vector<std::string>> lengthMap;

        for (stringMap::iterator it = map.begin(); it != map.end(); ++it) {
            std::string word = it->first;
            std::string clue = it->second;
            int len = word.size();

            lengthMap[len].push_back(word);
        }
        return lengthMap;
    }

    static void addPair(stringMap& cluesMap, std::string answer, std::string clue) {
        std::string formattedAnswer = "";
        for (int i=0; i<answer.size(); i++) {
            if (isalpha(answer[i])){ 
                formattedAnswer += std::toupper(answer[i]);
            }
        }
        cluesMap.insert(std::pair<std::string, std::string>(formattedAnswer, clue));
    }

    static stringMap readList() {
        std::map<std::string, std::string> cluesMap;
        std::string clue;
        std::string answer;

        std::string line;
        while (std::getline(std::cin, line))
        {
            int colonIndex = line.find(":");
            if (colonIndex == std::string::npos) {
                //No clue for this word
                clue = "";
                answer = line;
            } else {
                answer = line.substr(0, colonIndex);
                answer = removePunctuation(answer);
                clue = line.substr(colonIndex+1);
                clue = escapeQuotes(clue);
            }
            
            //Only add words 3+ letters in length
            if (answer.size() > 2) {
                Utilities::addPair(cluesMap, answer, clue);
            }
        }
        return cluesMap;
    }

    static std::string removePunctuation(std::string word) {
        word.erase(std::remove_if(word.begin(), word.end(), 
                  [](char ch){ return !::iswalnum(ch); }), word.end());
        return word;
    }

    static std::string escapeQuotes(std::string clue) {
        std::string normalQuote = "\"";
        std::string escapedQuote = "\\\"";

        size_t pos = clue.find(normalQuote);
        while(pos != std::string::npos)
        {
            clue.replace(pos, 1, escapedQuote);
            pos = clue.find(normalQuote, pos + 2);
        }
        return clue;
    }
};

#endif