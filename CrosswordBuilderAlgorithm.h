#ifndef CROSSWORDBUILDERALGORITHM
#define CROSSWORDBUILDERALGORITHM

#include <map>
#include <vector>
#include <string>

#include "Crossword.h"
#include "Utilities.h"

class CrosswordBuilderAlgorithm {
public:
    virtual ~CrosswordBuilderAlgorithm() {}
    virtual Crossword* build(stringMap wordsAndCluesMap) = 0;
    virtual Crossword* build(stringMap wordsAndCluesMap, int width, int height) {
        return build(wordsAndCluesMap);
    }
    bool supportsDynamicSizes() {
        return false;
    }
};

#endif
