#include "ValidPositionTracker.h"

ValidPositionTracker::ValidPositionTracker(int w, int h) {
    this->width = w;
    this->height = h;

    this->validStartDirections = initializeTemplate(w, h);
    this->validMiddleDirections = initializeTemplate(w, h);
    this->validEndDirections = initializeTemplate(w, h);
}

ValidPositionTracker::ValidPositionTracker(const ValidPositionTracker& other) {
    this->width = other.width;
    this->height = other.height;

    this->validStartDirections = initializeTemplate(width, height, other.validStartDirections);
    this->validMiddleDirections = initializeTemplate(width, height, other.validMiddleDirections);
    this->validEndDirections = initializeTemplate(width, height, other.validEndDirections);
}

ValidPositionTracker::~ValidPositionTracker() {
    for (int i = 0; i < this->width; i++) {
        delete[] this->validStartDirections[i];
        delete[] this->validMiddleDirections[i];
        delete[] this->validEndDirections[i];
    }
    delete[] this->validStartDirections;
    delete[] this->validMiddleDirections;
    delete[] this->validEndDirections;
}

int** ValidPositionTracker::initializeTemplate(int w, int h) {
    //Initialize all templates as valid at every position
    //This isn't really true for middle and end positions but these are invalidated later implicitly
    int** validDirections = new int*[h];
    for (int i = 0; i < h; i++) {
        validDirections[i] = new int[w];
        for (int j = 0; j < w; j++) {
            validDirections[i][j] = DOWN | ACROSS;
        }
    }
    return validDirections;
}

int** ValidPositionTracker::initializeTemplate(int w, int h, int** toCopy) {
    int** validDirections = new int*[h];
    for (int i = 0; i < w; i++) {
        validDirections[i] = new int[w];
        for (int j = 0; j < h; j++) {
            validDirections[i][j] = toCopy[i][j];
        }
    }
    return validDirections;
}

//TODO - short-circuit logic
bool ValidPositionTracker::isValidElement(GridElement element) {
    bool isValidStart = true;
    bool isValidMiddle = true;
    bool isValidEnd = true;
    if (isValidDirectionIndex(element.row, element.column)) {
        int validStartDirections = this->validStartDirections[element.row][element.column];
        isValidStart = (validStartDirections & element.direction) != NONE;
    } else {
        return false;
    }

    for (int i=1; i<element.word.size()-1; i++) {
        GridElement nextIndex = getNextElement(element, i);
        if (isValidDirectionIndex(nextIndex.row, nextIndex.column)) {
            int validMiddleDirections = this->validMiddleDirections[nextIndex.row][nextIndex.column];
            isValidMiddle = isValidMiddle && (validMiddleDirections & element.direction) != NONE;
        } else {
            return false;
        }
    }

    GridElement lastIndex = getNextElement(element, element.word.size()-1);
    if (isValidDirectionIndex(lastIndex.row, lastIndex.column)) {
        int validEndDirections = this->validEndDirections[lastIndex.row][lastIndex.column];
        isValidEnd = (validEndDirections & element.direction) != NONE;
    } else {
        return false;
    }

    return isValidStart && isValidMiddle && isValidEnd;
}

void ValidPositionTracker::markInvalidDirections(GridElement element) {
    if (element.direction == ACROSS) {
        markInvalidDirectionsAcross(element);
    } else if (element.direction == DOWN) {
        markInvalidDirectionsDown(element);
    }
}

void ValidPositionTracker::markInvalidDirectionsAcross(GridElement element) {
    int width = element.word.size();
    int** startTemplate = validPositionTemplate.get(ACROSS, FIRST, width);
    int** middleTemplate = validPositionTemplate.get(ACROSS, MIDDLE, width);
    int** endTemplate = validPositionTemplate.get(ACROSS, LAST, width);

    for (int row=-1; row<2; row++) {
        for (int col=-1; col<width+1; col++) {
            int rowIndex = element.row + row;
            int columnIndex = element.column + col;
            if (isValidDirectionIndex(rowIndex, columnIndex)) {
                //Template word starts at 1,1
                this->validStartDirections[rowIndex][columnIndex] &= startTemplate[1 + row][1 + col];
                this->validMiddleDirections[rowIndex][columnIndex] &= middleTemplate[1 + row][1 + col];
                this->validEndDirections[rowIndex][columnIndex] &= endTemplate[1 + row][1 + col];
            }
        }
    }
}

void ValidPositionTracker::markInvalidDirectionsDown(GridElement element) {
    int height = element.word.size();
    int** startTemplate = validPositionTemplate.get(DOWN, FIRST, height);
    int** middleTemplate = validPositionTemplate.get(DOWN, MIDDLE, height);
    int** endTemplate = validPositionTemplate.get(DOWN, LAST, height);

    for (int row=-1; row<height+1; row++) {
        for (int col=-1; col<2; col++) {
            int rowIndex = element.row + row;
            int columnIndex = element.column + col;
            if (isValidDirectionIndex(rowIndex, columnIndex)) {
                //Template word starts at 1,1
                this->validStartDirections[rowIndex][columnIndex] &= startTemplate[1 + row][1 + col];
                this->validMiddleDirections[rowIndex][columnIndex] &= middleTemplate[1 + row][1 + col];
                this->validEndDirections[rowIndex][columnIndex] &= endTemplate[1 + row][1 + col];
            }
        }
    }
}

GridElement ValidPositionTracker::getNextElement(GridElement element, int index){
    GridElement nextElement(element);
    switch (element.direction) {
        case ACROSS:
            nextElement.column += index;
        break;
        case DOWN:
            nextElement.row += index;
        break;
        default:
            //shouldn't happen
            std::cout << "Bad direction" << std::endl;
    }
    return nextElement;
}

bool ValidPositionTracker::isValidDirectionIndex(int row, int column) {
    return row >= 0 && row < this->height && column >= 0 && column < this->width;
}

/**
 * Helper function to draw the valid positions for debugging
 */
void ValidPositionTracker::draw() {
    std::cout << "Start: " << std::endl;
    for (int r=0; r<this->height; r++) {
        std::cout << "   ";
        for (int c=0; c<this->width; c++) {
            std::cout << this->validStartDirections[r][c] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "Middle: " << std::endl;
    for (int r=0; r<this->height; r++) {
        std::cout << "   ";
        for (int c=0; c<this->width; c++) {
            std::cout << this->validMiddleDirections[r][c] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "End: " << std::endl;
    for (int r=0; r<this->height; r++) {
        std::cout << "   ";
        for (int c=0; c<this->width; c++) {
            std::cout << this->validEndDirections[r][c] << " ";
        }
        std::cout << std::endl;
    }
}