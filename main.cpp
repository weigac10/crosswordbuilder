#ifdef LAYOUT_ALGORITHM
#include "algorithms/layout/LayoutCrosswordAlgorithm.h"
#endif
#ifdef TEMPLATE_ALGORITHM
#include "algorithms/template/TemplateCrosswordAlgorithm.h"
#endif

#include "Crossword.h"
#include "CrosswordBuilder.h"
#include "CrosswordElement.h"
#include "Utilities.h"

#include <map>
#include <string>
#include <ctype.h>

int main() {    
    stringMap cluesMap = Utilities::readList();

    CrosswordBuilderAlgorithm* builderAlgorithm = NULL;
#ifdef LAYOUT_ALGORITHM
    builderAlgorithm = new LayoutCrosswordAlgorithm();
#endif
#ifdef TEMPLATE_ALGORITHM
   builderAlgorithm = new TemplateCrosswordAlgorithm();
#endif

   CrosswordBuilder builder(builderAlgorithm);

    int puzzleNumber = 1;
    std::cout << "var puzzles = {";

    while (cluesMap.size() > 1000) {
        Crossword* crossword = builder.build(cluesMap);

        std::cout << std::endl << "\"puzzle" << puzzleNumber++ << "\":";
        crossword->printAsJSON();

        std::vector<CrosswordElement> elemsUsed = crossword->getElements();
        for (std::vector<CrosswordElement>::iterator it = elemsUsed.begin(); it != elemsUsed.end(); ++it) {
            cluesMap.erase((*it).gridElement.word);
        }
        if (cluesMap.size() > 1000) {
            std::cout << ",";
        }
        delete crossword;
    }
    // std::cout << "Words not used: " << std::endl;
    // for (std::map<std::string, std::string>::iterator it = cluesMap.begin(); it != cluesMap.end(); ++it) {
    //     std::cout << (*it).first << std::endl;
    // }
    std::cout << "};";

    delete builderAlgorithm;
}